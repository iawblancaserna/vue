import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { createAuth0 } from '@auth0/auth0-vue'

createApp(App)
    .use(router)
    .use(createAuth0({
        domain: 'dev-vinbres8.us.auth0.com',
        client_id: '5UbmUgm2czbVnoRDuGVztZTbsoGGYTyA',
        redirect_uri: window.location.origin,
        useRefreshTokens: true,
        cacheLocation: 'localstorage',
        audience: 'https://api-coments-blanca' // no es una url :)
    }))
    .mount('#app')