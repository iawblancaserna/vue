import { auth } from 'express-oauth2-jwt-bearer';

import { PSDB } from 'planetscale-node';

const conn = new PSDB('main')

export default async function handler(req, res) {
    
    req.is = () => false; 

    await auth()(req, res, (e) => {});

    if (req.auth === undefined) {
        res.status(401).send();
        return;        
    }
    req.auth.payload['https://my-awesome-namespace.com/user']

    const { query: { contenido } } = req

    const [rows, fields] = await conn.query(`insert into comentarios(contenido) values ('${contenido}')`)
    res.send(rows);
}